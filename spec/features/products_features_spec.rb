require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  scenario "user got to product_show" do
    visit potepan_product_path(product.id)
    expect(page).to have_css "div.header"
    expect(page).to have_css "div#footer"
    home_link = find(".navbar-brand")
    expect(home_link[:href]).to eq potepan_index_path
    expect(page).to have_link 'Home', href: potepan_index_path
  end
end
