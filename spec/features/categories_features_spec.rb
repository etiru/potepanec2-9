require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:ruby) { create(:taxon, name: "ruby", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) do
    create(:product, name: "RUBY BASEBALL JERSEY", taxons: [ruby])
  end

  scenario "The acquired taxon and taoxonomy and product is displayed " do
    visit potepan_category_path(taxonomy.root.id)
    expect(current_path).to eq potepan_category_path(taxonomy.root.id)
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content taxon.products.count
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario "When you click, you are transitioning to the specified link" do
    visit potepan_category_path(taxonomy.root.id)

    click_link ruby.name
    expect(current_path).to eq potepan_category_path(ruby.id)
    expect(page).to have_link ruby.name, href: potepan_category_path(ruby.id)

    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
