require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe 'Get request' do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully and 200 response" do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it 'respond show template' do
      expect(response).to render_template :show
    end
  end
end
