require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe 'Get request' do
    let!(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "responds successfully and 200 response" do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it 'showテンプレートで表示されること' do
      expect(response).to render_template :show
    end
  end
end
